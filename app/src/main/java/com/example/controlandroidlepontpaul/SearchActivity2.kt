package com.example.controlandroidlepontpaul

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.controlandroidlepontpaul.adapters.ListAppAdapter
import com.example.controlandroidlepontpaul.models.AppModel
import com.example.controlandroidlepontpaul.ui.utils.listOfApps
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_search2.*

class SearchActivity2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search2)
        var newInput : String? = null
        _searchEditText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                newInput = _searchEditText.text.toString()

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) =
                Unit

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })

        _buttonSearch.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    MainActivity::class.java
                )
            )
            intent.putExtra("textSearch", newInput)
        }
    }
}
