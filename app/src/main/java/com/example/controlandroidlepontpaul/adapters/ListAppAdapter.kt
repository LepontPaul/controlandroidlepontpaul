package com.example.controlandroidlepontpaul.adapters

import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.controlandroidlepontpaul.models.AppModel
import com.example.controlandroidlepontpaul.ui.utils.listOfApps
import com.facebook.drawee.view.SimpleDraweeView
import kotlinx.android.synthetic.main.list_app.view.*


class ListAppAdapter(val list: List<AppModel>) : RecyclerView.Adapter<ListAppAdapter.StringViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int): StringViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(
            com.example.controlandroidlepontpaul.R.layout.list_app,
            parent,
            false
        )
        return StringViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: StringViewHolder, position: Int) {
        holder.bind(list[position])
    }

    class StringViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        fun bind(appmodel:AppModel){
            val draweeView = view._imgApp as SimpleDraweeView
            draweeView.setImageURI("https://lh3.googleusercontent.com/Yc3-ehYHXE54cbv3uRk7sXNgWFhvvntl7gCEcLgzkaQJ_sPRxmQEiYfUurVgGtPm5BxN")
            view._stringNomApp.text = appmodel.nom
            view._stringGenreApp.text = appmodel.genre
            view._stringAnneeApp.text = appmodel.anneeSortie.toString()
        }
    }

}
