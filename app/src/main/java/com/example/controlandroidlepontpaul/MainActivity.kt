package com.example.controlandroidlepontpaul

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.controlandroidlepontpaul.adapters.ListAppAdapter
import com.example.controlandroidlepontpaul.models.AppModel
import com.example.controlandroidlepontpaul.ui.utils.listOfApps
import com.example.controlandroidlepontpaul.ui.utils.sortByAnnee
import com.example.controlandroidlepontpaul.ui.utils.sortByGenre
import com.facebook.drawee.backends.pipeline.Fresco
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Fresco.initialize(this)
        val listOfApp : List<AppModel> = listOfApps

        _listRecycler.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = ListAppAdapter(listOfApp)
        }
        _buttonFiltreGenre.setOnClickListener {
            var listSortByGenre = sortByGenre()
            _listRecycler.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = ListAppAdapter(listSortByGenre)
            }
        }
        _buttonFiltreAnnee.setOnClickListener {
            var listSortByAnnee = sortByAnnee()
            _listRecycler.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = ListAppAdapter(listSortByAnnee)
            }
        }
        _buttonRechercher.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    SearchActivity2::class.java
                )
            )
        }
        val intent = intent
        val value = intent.getStringExtra("textSearch")
// ------------------ Ici on testerai si la value existe ou pas et si il existe on tri par rapport au nom ------------------------
//        Log.d("debug", value)
//        if(value.isNotBlank()){
//            listOfApp.filter {
//                it.nom == value
//            }
//            Log.d("debug", listOfApp.toString())
//        }
    }

}
