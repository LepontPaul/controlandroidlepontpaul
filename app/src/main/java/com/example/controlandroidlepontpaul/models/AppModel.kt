package com.example.controlandroidlepontpaul.models

import android.util.Log
import com.example.controlandroidlepontpaul.ui.utils.listOfApps

class AppModel(
    val nom: String,
    val genre: String,
    val anneeSortie: Int,
    val imageUrl: String
) {
    override fun toString(): String {
        return "AppModel(nom='$nom', genre='$genre', anneeSortie=$anneeSortie)"
    }
}